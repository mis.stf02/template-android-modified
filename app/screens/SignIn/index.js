import { AuthActions } from "@actions";
import { Button, SafeAreaView, Text, TextInput } from "@components";
import { BaseColor, BaseStyle, useTheme } from "@config";
import { Images } from "@config";
import React, { useState } from "react";
import {
  ScrollView,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { useDispatch, connect } from "react-redux";
import styles from "./styles";
import { useTranslation } from "react-i18next";

const { login } = AuthActions;
const successInit = {
  id: true,
  password: true,
};

const SignIn = (props) => {
  const { navigation } = props;
  const { t } = useTranslation();
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const [id, setId] = useState("LNK000664");
  const [password, setPassword] = useState("secret");
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(successInit);

  const onLogin = async () => {
    if (id == "" || password == "") {
      setSuccess({
        ...success,
        id: false,
        password: false,
      });
    } else {
      setLoading(true);
      dispatch(login(id, password))
        .then((res) => {
          if (props.isLoggedIn) {
            // navigate to home

            navigation.navigate("ProjectMenu");
          }
          setLoading(false);
        })
        .catch((e) => {
          setLoading(false);
        });
    }
  };

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  return (
    <SafeAreaView
      style={BaseStyle.safeAreaView}
      edges={["right", "top", "left"]}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        keyboardVerticalOffset={offsetKeyboard}
        style={{
          flex: 1,
        }}
      >
        <View
          style={{ alignItems: "center", flex: 0.7, justifyContent: "center" }}
        >
          <Image
            source={Images.logolnk}
            style={{
              width: 220,
              height: 220,
            }}
            resizeMode="contain"
          />
        </View>
        <View style={styles.contain}>
          <TextInput
            style={[BaseStyle.textInput]}
            onChangeText={(text) => setId(text)}
            onFocus={() => {
              setSuccess({
                ...success,
                id: true,
              });
            }}
            autoCorrect={false}
            placeholder={t("Username")}
            placeholderTextColor={
              success.id ? BaseColor.grayColor : colors.primary
            }
            value={id}
            selectionColor={colors.primary}
          />
          <TextInput
            style={[BaseStyle.textInput, { marginTop: 10 }]}
            onChangeText={(text) => setPassword(text)}
            onFocus={() => {
              setSuccess({
                ...success,
                password: true,
              });
            }}
            autoCorrect={false}
            placeholder={t("Password")}
            secureTextEntry={true}
            placeholderTextColor={
              success.password ? BaseColor.grayColor : colors.primary
            }
            value={password}
            selectionColor={colors.primary}
          />
          <View style={{ width: "100%", marginVertical: 16 }}>
            <Button
              full
              loading={loading}
              style={{ marginTop: 20 }}
              onPress={onLogin}
            >
              {t("sign_in")}
            </Button>
          </View>
          <View style={styles.contentActionBottom}>
            <TouchableOpacity
              onPress={() => navigation.navigate("ResetPassword")}
            >
              <Text body2 grayColor>
                {t("forgot_your_password")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};
function mapStateToProps(state) {
  const { isLoggedIn } = state.auth;
  const { message } = state.message;
  return {
    message,
    isLoggedIn,
  };
}
export default connect(mapStateToProps)(SignIn);
