import axios from "axios";
import { BASE_URL, PUBLIC_KEY, APP_NAME } from "@env";
import AsyncStorage from "@react-native-async-storage/async-storage";

const publicKey = PUBLIC_KEY;
const appName = APP_NAME;
const login = async (username, password) => {
  const StorageToken = await AsyncStorage.getItem("appToken");
  const appToken = StorageToken ? StorageToken : await generateToken();
  const decodeAppToken = JSON.parse(appToken);

  const headers = {
    "x-public-key": publicKey,
    "x-app-name": appName,
    "Content-Type": "application/json",
    "X-Application-Token": `Bearer ${decodeAppToken.appToken}`,
  };
  return axios
    .post(
      `${BASE_URL}auth/signin`,
      { username, password },
      { headers: headers }
    )
    .then(async (response) => {
      await AsyncStorage.setItem("accessToken", JSON.stringify(response.data));

      return response;
    });
};

const generateToken = async () => {
  const headers = {
    "x-public-key": publicKey,
    "x-app-name": appName,
    "Content-Type": "application/json",
  };
  return axios
    .post(
      `${BASE_URL}auth/app-token`,
      {},
      {
        headers: headers,
      }
    )
    .then(async (response) => {
      if (response.data.appToken) {
        await AsyncStorage.setItem("appToken", JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = async () => {
  return await AsyncStorage.clear();
};

export default { login, generateToken, logout };
