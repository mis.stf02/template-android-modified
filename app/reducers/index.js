import { combineReducers } from "redux";
import auth from "./auth";
import ApplicationReducer from "./application";
import message from "./message";

export default combineReducers({
  auth,
  application: ApplicationReducer,
  message,
});
