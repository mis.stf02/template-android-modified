import AsyncStorage from "@react-native-async-storage/async-storage";
import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../actions/customTypes";
import * as actionTypes from "@actions/actionTypes";

const userPayload = AsyncStorage.getItem("accessToken");
const initialState = userPayload
  ? {
      isLoggedIn: true,
      user: {
        lang: "en",
      },
      login: {
        success: true,
      },
      userPayload,
    }
  : {
      login: {
        success: false,
      },
      isLoggedIn: false,
      user: {
        lang: "en",
      },
      userPayload: null,
    };

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.LOGIN:
      return {
        login: action.data,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        userPayload: payload.user.accessToken,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        userPayload: null,
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        userPayload: null,
      };
    default:
      return state;
  }
};
